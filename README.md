# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.2.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.2.2/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#web)
* [Liquibase Migration](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#howto.data-initialization.migration-tool.liquibase)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#actuator)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

## Run application

### mock environment
first start the mock environment see README in repo 

### start database
```bash
docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=1Gjki673j -d postgres
docker exec -it postgresql-container psql -U postgres -c "create database pdf"
```

### Build application
```bash
mvn clean install
```

### Start application
```bash
mvn spring-boot:run
```

### Health check
/form_generator/actuator/health

### add user

```bash
curl --location 'http://localhost:8082/form_generator/auth/register' \
--header 'Content-Type: application/json' \
--data-raw '{
"firstName": "bruce",
"lastName": "wayne",
"login": "brucewayne@momentum.co.za",
"password": "91939"
}'

```

