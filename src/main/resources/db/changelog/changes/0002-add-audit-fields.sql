--liquibase formatted sql

--changeset MigalLewis:0002-add-audit-fields
CREATE TABLE audit_log (
    id BIGSERIAL PRIMARY KEY,
    table_name VARCHAR(255),
    entity_id BIGSERIAL,
    operation_type VARCHAR(255),
    details TEXT,
    changedBy VARCHAR(255),
    last_modified_by VARCHAR(255),
    changeTime TIMESTAMP
);

ALTER TABLE pdf_user
ADD COLUMN created_by VARCHAR(255),
ADD COLUMN last_modified_by VARCHAR(255),
ADD COLUMN created_date TIMESTAMP,
ADD COLUMN last_modified_date TIMESTAMP;