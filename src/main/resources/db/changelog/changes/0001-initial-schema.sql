--liquibase formatted sql

--changeset MigalLewis:1
CREATE TABLE pdf_user (
    id BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(255),
    lastName VARCHAR(255),
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);