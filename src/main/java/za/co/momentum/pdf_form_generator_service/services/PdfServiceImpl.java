package za.co.momentum.pdf_form_generator_service.services;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class PdfServiceImpl implements PdfService {

    @Override
    public ByteArrayInputStream createPdfForm(List<List<String>> rows) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        log.info("creating pdf Form ");
        log.debug("rows "+rows);

        try(PdfWriter pdfWriter = new PdfWriter(outputStream);
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            Document document = new Document(pdfDocument)) {

            PdfAcroForm form = PdfAcroForm.getAcroForm(document.getPdfDocument(), true);
            pdfDocument.addNewPage();

            ImageData imageData = ImageDataFactory.create("src/main/resources/images/momentum.png");
            Image image = new Image(imageData).scaleAbsolute(270,104);
            image.setFixedPosition(1, 0, PageSize.A4.getHeight()-image.getImageScaledHeight());

            document.add(image);

            // Assuming first row is header names
            createFormFields(pdfDocument, form, rows.get(0), 150);
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputStream.toByteArray());
        log.debug("after creating form");
        log.debug("byte size "+byteArrayInputStream.available());
        return  byteArrayInputStream;
    }

    public void createFormFields(PdfDocument pdfDocument, PdfAcroForm form , List<String> headers, float startingPointY) throws IOException {

        float x = 200;

        int width = 300;
        int height = 40;


        PdfFont font = PdfFontFactory.createFont();
        PdfFont boldFont = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);

        PdfCanvas request = new PdfCanvas(pdfDocument.getFirstPage());
        request.beginText();
        request.setFontAndSize(boldFont, 12);

        request.moveText(x - 160, PageSize.A4.getHeight() - startingPointY + 20);
        request.showText("Please fill in the form : ");
        request.endText();


        for( int i = 0; i < headers.size(); i++) {
            String header = headers.get(i);
            float y =  PageSize.A4.getHeight() - startingPointY - 40;
            y = y - (60 * i);

            PdfCanvas canvas = new PdfCanvas(pdfDocument.getFirstPage());
            canvas.beginText();
            canvas.setFontAndSize(font, 18);
            canvas.moveText(x - 160, y + 20);
            canvas.showText(header + " : ");
            canvas.endText();

            Rectangle rectangle = new Rectangle(x, y, width, height);

            PdfFormField field = PdfFormField.createText(pdfDocument, rectangle, header);
            log.trace("added header "+header);

            field.setBorderColor(new DeviceRgb(0, 0, 0));
            field.setBackgroundColor(new DeviceRgb(192, 192, 192));
            field.setBorderWidth(1);
            field.setColor(new DeviceRgb(0, 0, 0));

            form.addField(field);
        }
    }

    public ByteArrayInputStream fillInForm(ByteArrayInputStream pdfTemplateFile, List<String> rowData) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                PdfDocument pdfDocument = new PdfDocument(new PdfReader(pdfTemplateFile), new PdfWriter(outputStream))) {

            PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDocument, true);

            for (int i = 0; i < rowData.size(); i++) {
                PdfString header = form.getFormFields().get(i).getFieldName();
                String value = rowData.get(i);
                log.trace("header value: "+header.getValue());
                log.trace("header toString: "+header.toString());
                form.getField(header.getValue()).setValue(value);
            }
            byte[] byteArray = outputStream.toByteArray();
            return new ByteArrayInputStream(byteArray);

        } catch (IOException e) {
            log.error("Error reading pdf file", e);
        }
        return pdfTemplateFile;
    }

}
