package za.co.momentum.pdf_form_generator_service.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public interface PdfService {
    ByteArrayInputStream createPdfForm(List<List<String>> fields) throws IOException;
    ByteArrayInputStream fillInForm(ByteArrayInputStream pdfTemplateFile, List<String> rowData) throws IOException;
}
