package za.co.momentum.pdf_form_generator_service.services;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;
import za.co.momentum.pdf_form_generator_service.mappers.UserMapper;
import za.co.momentum.pdf_form_generator_service.models.CredentialsDto;
import za.co.momentum.pdf_form_generator_service.models.SignUpDto;
import za.co.momentum.pdf_form_generator_service.models.UserDto;
import za.co.momentum.pdf_form_generator_service.repo.UserRepository;
import za.co.momentum.pdf_form_generator_service.repo.entities.User;

import java.nio.CharBuffer;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    @Override
    public UserDto login(CredentialsDto credentialsDto) {
        User user = userRepository.findByLogin(credentialsDto.login()).orElseThrow(
                ()-> new PdfFormException("Unknown user", HttpStatus.NOT_FOUND)
        );
        if(passwordEncoder.matches(CharBuffer.wrap(credentialsDto.password()), user.getPassword())){
            return userMapper.toUserDto(user);
        }
        throw new PdfFormException("Invalid Password", HttpStatus.BAD_REQUEST);
    }

    @Override
    public UserDto register(SignUpDto signUpDto) {
        Optional<User> optionalUser = userRepository.findByLogin(signUpDto.login());
        if(optionalUser.isPresent()){
            throw new PdfFormException("User already exists", HttpStatus.BAD_REQUEST);
        }

        User user = userMapper.signUpToUser(signUpDto);
        user.setPassword(passwordEncoder.encode(CharBuffer.wrap(signUpDto.password())));
        User savedUser = userRepository.save(user);
        return userMapper.toUserDto(savedUser);
    }
}
