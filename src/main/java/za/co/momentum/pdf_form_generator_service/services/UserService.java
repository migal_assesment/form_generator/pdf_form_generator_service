package za.co.momentum.pdf_form_generator_service.services;

import za.co.momentum.pdf_form_generator_service.models.CredentialsDto;
import za.co.momentum.pdf_form_generator_service.models.SignUpDto;
import za.co.momentum.pdf_form_generator_service.models.UserDto;

public interface UserService {
    UserDto login(CredentialsDto credentialsDto);
    UserDto register(SignUpDto signUpDto);
}
