package za.co.momentum.pdf_form_generator_service.services;

import za.co.momentum.pdf_form_generator_service.models.PerformanceLogComparatorDto;

import java.util.List;

public interface FileServicePerformanceComparator {
    PerformanceLogComparatorDto compareAndLogPerformance();

    List<String> fileNames();
    byte[] gatFile(String fileName) ;
}
