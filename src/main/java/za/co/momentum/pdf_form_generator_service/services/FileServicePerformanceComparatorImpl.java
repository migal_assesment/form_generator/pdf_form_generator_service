package za.co.momentum.pdf_form_generator_service.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import za.co.momentum.pdf_form_generator_service.mappers.PerformanceMapper;
import za.co.momentum.pdf_form_generator_service.models.PerformanceLogComparatorDto;
import za.co.momentum.pdf_form_generator_service.models.PerformanceLogDto;
import za.co.momentum.pdf_form_generator_service.repo.PerformanceLogRepository;
import za.co.momentum.pdf_form_generator_service.repo.entities.PerformanceLog;

import java.util.List;

@RequiredArgsConstructor
@Service
public class FileServicePerformanceComparatorImpl implements FileServicePerformanceComparator {
    @Qualifier("fileServiceImpl")
    private final FileService fileServiceImpl;
    @Qualifier("s3FileServiceImpl")
    private final FileService s3FileServiceImpl;
    private final PerformanceLogRepository performanceLogRepository;
    private final PerformanceMapper performanceMapper;

    public PerformanceLogComparatorDto compareAndLogPerformance() {
        long startFileServiceImpl = System.nanoTime();
        fileServiceImpl.fetchCsvCreatePdfAndSend();
        long durationFileServiceImpl = System.nanoTime() - startFileServiceImpl;

        long startS3FileServiceImpl = System.nanoTime();
        s3FileServiceImpl.fetchCsvCreatePdfAndSend();
        long durationS3FileServiceImpl = System.nanoTime() - startS3FileServiceImpl;

        PerformanceLog performanceLogFileServiceImpl = PerformanceLog.builder()
                .methodName("fetchCsvCreatePdfAndSend")
                .serviceName("fileServiceImpl")
                .durationNano(durationFileServiceImpl)
                .build();

        PerformanceLog performanceLogS3FileService = PerformanceLog.builder()
                .methodName("fetchCsvCreatePdfAndSend")
                .serviceName("s3FileServiceImpl")
                .durationNano(durationS3FileServiceImpl)
                .build();

        PerformanceLogDto dtoFileService = performanceMapper.toPerformanceLogDto(performanceLogFileServiceImpl);
        dtoFileService.setServiceName("File server");
        PerformanceLogDto dtoS3FileService = performanceMapper.toPerformanceLogDto(performanceLogS3FileService);
        dtoS3FileService.setServiceName("S3 bucket");
        dtoFileService.isWinner(dtoS3FileService.getDurationNano());
        dtoS3FileService.isWinner(dtoFileService.getDurationNano());


        performanceLogRepository.save(performanceLogFileServiceImpl);
        performanceLogRepository.save(performanceLogS3FileService);

        return PerformanceLogComparatorDto.builder().s3(dtoS3FileService).fileServer(dtoFileService).build();
    }

    @Override
    public List<String> fileNames() {
        return fileServiceImpl.getFilenames();
    }

    @Override
    public byte[] gatFile(String fileName) {
        return fileServiceImpl.getFile(fileName);
    }


}
