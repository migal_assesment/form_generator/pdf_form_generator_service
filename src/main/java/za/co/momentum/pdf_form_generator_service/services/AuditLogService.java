package za.co.momentum.pdf_form_generator_service.services;

public interface AuditLogService {
    void logChange(String tableName, Long entityId, String operationType, String details, String changedBy);
}
