package za.co.momentum.pdf_form_generator_service.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;
import za.co.momentum.pdf_form_generator_service.util.FileUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
public class S3FileServiceImpl implements FileService{

    @Value("${files.server.csv.url}")
    String csvUrl;
    @Value("${files.server.names.url}")
    String getFilenames;
    @Value("${s3.bucket}")
    private String bucketName;

    private final RestTemplate restTemplate;
    private final PdfService pdfService;
    private final S3Client s3Client;
    private final FileUtil fileUtil;

    @Override
    public void fetchCsvCreatePdfAndSend() {
        log.info("Fetching Csv");
        log.debug("csvUrl : "+ csvUrl);

        String csvContent = restTemplate.getForObject(csvUrl, String.class);
        log.debug("csvContent : "+ csvContent);
        if (csvContent != null && !csvContent.isEmpty()) {
            try {
                List<List<String>> data =  getDataFromCsvFile(csvContent);
                log.debug("csv content : "+ data);
                ByteArrayInputStream byteArrayInputStream = pdfService.createPdfForm(data);
                sendPdfToS3(byteArrayInputStream, fileUtil.getFileNameWithoutExtension(csvUrl)+".pdf");
            } catch (IOException e) {
                log.error("Failed to create pdf ", e);
            }
        } else {
            log.error("Failed to fetch file ");
            throw new PdfFormException("Failed to fetch file", HttpStatus.BAD_REQUEST);
        }
    }

    public void sendPdfToS3(ByteArrayInputStream byteArrayInputStream, String fileName) throws IOException {
        log.info("send Pdf To S3 bucket : "+ bucketName);
        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .contentType("application/pdf")
                .build();

        s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(byteArrayInputStream, byteArrayInputStream.available()));
    }

    @Override
    public List<List<String>> getDataFromCsvFile(String csvContent) {
        String[] lines = csvContent.split("\n");

        List<List<String>> csvData = new ArrayList<>();
        for (String line : lines) {
            if(line.isEmpty())
            {
                log.debug("skip line");
            } else {
                List<String> parsedLine = Arrays.asList(line.split(","));
                csvData.add(parsedLine);
            }
        }

        return csvData;
    }

    @Override
    public List<String> getFilenames() {
        String[] filenames = restTemplate.getForObject(getFilenames, String[].class);
        return Arrays.asList(filenames);
    }

    @Override
    public byte[] getFile(String filename) {
        String url = getFilenames + "/{filename}";
        return restTemplate.getForObject(url, byte[].class, filename);
    }

}
