package za.co.momentum.pdf_form_generator_service.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import za.co.momentum.pdf_form_generator_service.repo.AuditLogRepository;
import za.co.momentum.pdf_form_generator_service.repo.entities.AuditLog;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class AuditLogServiceInterface implements AuditLogService{

    private final AuditLogRepository auditLogRepository;
    @Override
    public void logChange(String tableName, Long entityId, String operationType, String details, String changedBy) {
        AuditLog auditLog = new AuditLog();
        auditLog.setTableName(tableName);
        auditLog.setEntityId(entityId);
        auditLog.setOperationType(operationType);
        auditLog.setChangedBy(changedBy);
        auditLog.setDetails(details);
        auditLog.setChangeTime(LocalDateTime.now());
        auditLogRepository.save(auditLog);
    }
}
