package za.co.momentum.pdf_form_generator_service.services;

import java.util.List;

public interface FileService {

    public void fetchCsvCreatePdfAndSend();
    public List<List<String>> getDataFromCsvFile(String csvContent) ;

    public List<String> getFilenames();

    public byte[] getFile(String name);
}
