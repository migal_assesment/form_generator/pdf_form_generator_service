package za.co.momentum.pdf_form_generator_service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;

import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public class FileUtil {

    public String getFileNameWithoutExtension(String urlString) {
        try {
            URL url = new URL(urlString);
            String fileName = new URL(url.toString()).getPath();
            fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
            String fileNameWithoutExt = fileName.substring(0, fileName.lastIndexOf('.'));
            return fileNameWithoutExt;
        } catch (MalformedURLException e) {
            log.error("Failed to create url ", e);
            throw new PdfFormException("Failed to create url ", HttpStatus.BAD_REQUEST);
        }
    }
}
