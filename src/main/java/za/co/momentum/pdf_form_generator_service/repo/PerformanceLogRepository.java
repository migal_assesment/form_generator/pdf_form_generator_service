package za.co.momentum.pdf_form_generator_service.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.momentum.pdf_form_generator_service.repo.entities.PerformanceLog;

@Repository
public interface PerformanceLogRepository extends JpaRepository<PerformanceLog, Long> {
}
