package za.co.momentum.pdf_form_generator_service.repo.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AuditLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tableName;
    private Long entityId;
    private String operationType;
    private String changedBy;
    private LocalDateTime changeTime;
    @Column(length = 10000)
    private String details;
}
