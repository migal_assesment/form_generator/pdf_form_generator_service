package za.co.momentum.pdf_form_generator_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class PdfFormGeneratorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfFormGeneratorServiceApplication.class, args);
	}

}
