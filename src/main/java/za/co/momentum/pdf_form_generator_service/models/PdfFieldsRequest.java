package za.co.momentum.pdf_form_generator_service.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class PdfFieldsRequest {
    private String fileName;
    private List<List<String>> data;
}
