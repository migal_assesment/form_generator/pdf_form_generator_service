package za.co.momentum.pdf_form_generator_service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PerformanceLogDto {
    private String methodName;
    private String serviceName;
    private Long durationNano;
    private boolean winner;

    public boolean isWinner(Long competitorDuration) {
        winner = durationNano < competitorDuration ;
        return winner;
    }
}
