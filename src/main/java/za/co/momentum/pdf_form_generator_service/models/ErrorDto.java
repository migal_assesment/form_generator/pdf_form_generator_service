package za.co.momentum.pdf_form_generator_service.models;

public record ErrorDto(String message) {
}
