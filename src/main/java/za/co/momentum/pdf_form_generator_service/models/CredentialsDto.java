package za.co.momentum.pdf_form_generator_service.models;

public record CredentialsDto(String login, char[] password){

}
