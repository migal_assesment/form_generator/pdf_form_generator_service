package za.co.momentum.pdf_form_generator_service.models;

public record SignUpDto(String firstName, String lastName, String login, char[] password) {}
