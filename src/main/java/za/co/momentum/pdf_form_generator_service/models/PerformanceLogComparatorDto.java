package za.co.momentum.pdf_form_generator_service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PerformanceLogComparatorDto {
    PerformanceLogDto s3;
    PerformanceLogDto fileServer;

}
