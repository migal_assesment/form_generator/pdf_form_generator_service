package za.co.momentum.pdf_form_generator_service.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.momentum.pdf_form_generator_service.models.PerformanceLogComparatorDto;
import za.co.momentum.pdf_form_generator_service.services.FileServicePerformanceComparator;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/files")
public class FileController {
    private final FileServicePerformanceComparator fileServicePerformanceComparator;

    @PostMapping("/createForms")
    public ResponseEntity<PerformanceLogComparatorDto> getCsvFileCreateFormAndUpload(){
        try {
            log.info("fetching and sending");
            PerformanceLogComparatorDto performanceLogComparatorDto = fileServicePerformanceComparator.compareAndLogPerformance();
            HttpHeaders headers = new HttpHeaders();

            return new ResponseEntity<>(performanceLogComparatorDto, headers, HttpStatus.OK);
        } catch(Exception e) {
            log.error("Something went wrong ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/filename")
    public ResponseEntity<List<String>> getFilenames(){
        try {
            log.info("fetching and sending");
            List<String> fileNames = fileServicePerformanceComparator.fileNames();
            HttpHeaders headers = new HttpHeaders();

            return new ResponseEntity<>(fileNames, headers, HttpStatus.OK);
        } catch(Exception e) {
            log.error("Something went wrong ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/file/")
    public ResponseEntity<byte[]> getFile(@RequestParam String fileName){
        try {
            log.info("fetching and sending");
            byte[] file = fileServicePerformanceComparator.gatFile(fileName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDispositionFormData("filename", fileName);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

            return new ResponseEntity<>(file, headers, HttpStatus.OK);
        } catch(Exception e) {
            log.error("Something went wrong ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}
