package za.co.momentum.pdf_form_generator_service.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.momentum.pdf_form_generator_service.configurations.UserAuthProvider;
import za.co.momentum.pdf_form_generator_service.models.CredentialsDto;
import za.co.momentum.pdf_form_generator_service.models.PdfFieldsRequest;
import za.co.momentum.pdf_form_generator_service.models.SignUpDto;
import za.co.momentum.pdf_form_generator_service.models.UserDto;
import za.co.momentum.pdf_form_generator_service.services.PdfService;
import za.co.momentum.pdf_form_generator_service.services.UserService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;
    private final UserAuthProvider userAuthProvider;

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody CredentialsDto credentialsDto) {
        UserDto userDto = userService.login(credentialsDto);
        userDto.setToken(userAuthProvider.createToken(userDto));
        return new ResponseEntity<>(userDto, null, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> register(@RequestBody SignUpDto signUpDto) {
        UserDto userDto = userService.register(signUpDto);
        userDto.setToken(userAuthProvider.createToken(userDto));
        return  ResponseEntity.created(URI.create("/users/"+ userDto.getId())).body(userDto);
    }


}
