package za.co.momentum.pdf_form_generator_service.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.momentum.pdf_form_generator_service.models.PdfFieldsRequest;
import za.co.momentum.pdf_form_generator_service.services.PdfService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/pdf")
public class PdfController {
    private PdfService pdfService;

    public PdfController(PdfService pdfService) {
        this.pdfService = pdfService;
    }

    @PostMapping("/generate")
    public ResponseEntity<byte[]> generatePdfForm(@RequestBody PdfFieldsRequest pdfFieldsRequest) throws IOException {
        ByteArrayInputStream byteArrayInputStream = pdfService.createPdfForm(pdfFieldsRequest.getData());

        byte[] pdfContents = byteArrayInputStream.readAllBytes();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("filename", pdfFieldsRequest.getFileName());
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(pdfContents, headers, HttpStatus.OK);
    }

    @GetMapping("/fillInForm")
    public ResponseEntity<byte[]> fillInPdfForm(@RequestParam("pdfFile") MultipartFile pdfFile,
                                                @RequestParam("rowData") List<String> rowData) throws IOException {

        ByteArrayInputStream pdfTemplateFile = new ByteArrayInputStream(pdfFile.getBytes());
        ByteArrayInputStream byteArrayInputStream = pdfService.fillInForm(pdfTemplateFile, rowData);

        byte[] pdfContents = byteArrayInputStream.readAllBytes();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("filename", pdfFile.getName());
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(pdfContents, headers, HttpStatus.OK);
    }

    @GetMapping("/test")
    public ResponseEntity<Boolean> test() throws IOException {
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<>(true,headers,HttpStatus.OK);
    }

}
