package za.co.momentum.pdf_form_generator_service.mappers;

import org.mapstruct.Mapper;
import za.co.momentum.pdf_form_generator_service.models.PerformanceLogDto;
import za.co.momentum.pdf_form_generator_service.repo.entities.PerformanceLog;

@Mapper(componentModel = "spring")
public interface PerformanceMapper {

    PerformanceLogDto toPerformanceLogDto(PerformanceLog performanceLog);
}
