package za.co.momentum.pdf_form_generator_service.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import za.co.momentum.pdf_form_generator_service.models.SignUpDto;
import za.co.momentum.pdf_form_generator_service.models.UserDto;
import za.co.momentum.pdf_form_generator_service.repo.entities.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto toUserDto(User user);
    @Mapping(target = "password", ignore = true)
    User signUpToUser(SignUpDto signUpDto);

}
