package za.co.momentum.pdf_form_generator_service.exceptions;

import org.springframework.http.HttpStatus;

public class PdfFormException extends RuntimeException {
    private final HttpStatus httpStatus;
    public PdfFormException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
