package za.co.momentum.pdf_form_generator_service.configurations;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;
import software.amazon.awssdk.services.s3.S3Client;
import za.co.momentum.pdf_form_generator_service.services.FileService;
import za.co.momentum.pdf_form_generator_service.services.FileServiceImpl;
import za.co.momentum.pdf_form_generator_service.services.PdfService;
import za.co.momentum.pdf_form_generator_service.services.S3FileServiceImpl;
import za.co.momentum.pdf_form_generator_service.util.FileUtil;

import java.util.Optional;

@RequiredArgsConstructor
@Configuration
public class BeanConfig {

    @Bean
    public RestTemplate restTemplate() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        return restTemplateBuilder.build();
    }

    @Bean
    public FileUtil fileUtil() {
        return new FileUtil();
    }

    @Bean
    public AuditorAware<String> auditorProvider() {
        return () -> Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getName());
    }
    @Bean
    @Qualifier("fileServiceImpl")
    public FileService fileServiceImpl(RestTemplate restTemplate,
                                       PdfService pdfService,
                                       FileUtil fileUtil
                                       ) {
        return new FileServiceImpl(restTemplate, pdfService, fileUtil);
    }

    @Bean
    @Qualifier("s3FileServiceImpl")
    public FileService s3FileServiceImpl(RestTemplate restTemplate,
                                         PdfService pdfService,
                                         S3Client s3Client,
                                         FileUtil fileUtil) {
        return new S3FileServiceImpl(restTemplate, pdfService, s3Client, fileUtil);
    }
}
