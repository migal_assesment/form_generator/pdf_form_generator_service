package za.co.momentum.pdf_form_generator_service.configurations;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import za.co.momentum.pdf_form_generator_service.repo.entities.Auditable;
import za.co.momentum.pdf_form_generator_service.repo.entities.BaseEntity;
import za.co.momentum.pdf_form_generator_service.services.AuditLogService;

@Aspect
@Component
@RequiredArgsConstructor
public class AuditAspect {

    private final AuditLogService auditLogService;
    private final AuditorAware<String> auditorAware;

    @Pointcut("execution(* org.springframework.data.repository.CrudRepository+.save(..)) || execution(* org.springframework.data.repository.CrudRepository+.delete(..))")
    public void repositoryOperations() {}

    @AfterReturning("repositoryOperations()")
    public void logAfterReturning(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Object entity = args[0];

        if (entity instanceof Auditable) {
            String operationType = joinPoint.getSignature().getName().toUpperCase();
            String tableName = entity.getClass().getSimpleName();
            Long entityId = null;
            if (entity instanceof BaseEntity) {
                entityId = ((BaseEntity) entity).getId();
            }
            String details = entity.toString();
            String changedBy = auditorAware.getCurrentAuditor().orElse("Unknown");

            auditLogService.logChange(tableName, entityId, operationType, details, changedBy);
        }
    }

}
