package za.co.momentum.pdf_form_generator_service.configurations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;
import za.co.momentum.pdf_form_generator_service.models.ErrorDto;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = {PdfFormException.class})
    @ResponseBody
    public ResponseEntity<ErrorDto> handleException(PdfFormException pfe){
        return ResponseEntity.status(pfe.getHttpStatus())
                .body(new ErrorDto(pfe.getMessage()));
    }
}
