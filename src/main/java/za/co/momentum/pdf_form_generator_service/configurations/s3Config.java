package za.co.momentum.pdf_form_generator_service.configurations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.waiters.S3Waiter;

import java.net.URI;

@Configuration
@Slf4j
public class s3Config {
    @Value("${s3.endpoint}")
    private String s3Endpoint;

    @Value("${cloud.aws.region.static}")
    private String region;

    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${s3.bucket}")
    private String bucketName;

    @Bean
    public S3Client s3Client() {
        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(accessKey, secretKey);

        S3Client s3 = S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .endpointOverride(URI.create(s3Endpoint))
                .region(Region.of(region))
                .serviceConfiguration(S3Configuration.builder().pathStyleAccessEnabled(true).build())
                .build();

        createBucket(s3, bucketName);
        return s3;

    }

    public void createBucket(S3Client s3Client, String bucketName) {
        try {
            ListBucketsResponse listBucketsResponse = s3Client.listBuckets();
            boolean bucketExists = listBucketsResponse.buckets().stream()
                    .anyMatch(b -> b.name().equals(bucketName));
            if (bucketExists) {
                log.debug("Bucket " + bucketName + " already exists.");
                return;
            }

            CreateBucketRequest bucketRequest = CreateBucketRequest.builder()
                    .bucket(bucketName)
                    .build();

            s3Client.createBucket(bucketRequest);
            log.debug("Bucket " + bucketName + " has been created.");

            waitForBucketToExist(s3Client, bucketName);

        } catch (S3Exception e) {
            log.error("Error in creating bucket: " + e.awsErrorDetails().errorMessage(), e);
        }
    }

    private void waitForBucketToExist(S3Client s3Client, String bucketName) {
        S3Waiter s3Waiter = s3Client.waiter();
        HeadBucketRequest bucketRequestWait = HeadBucketRequest.builder()
                .bucket(bucketName)
                .build();

        WaiterResponse<HeadBucketResponse> waiterResponse = s3Waiter.waitUntilBucketExists(bucketRequestWait);
        waiterResponse.matched().response().ifPresent(present -> {
            log.info("Bucket " + bucketName + " is now present and ready for use.");
        });
    }

}
