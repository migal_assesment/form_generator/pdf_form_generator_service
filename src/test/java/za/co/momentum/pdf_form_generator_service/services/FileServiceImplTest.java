package za.co.momentum.pdf_form_generator_service.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import za.co.momentum.pdf_form_generator_service.util.FileUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.mockito.Mockito.*;

class FileServiceImplTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PdfService pdfService;

    @Mock
    private FileUtil fileUtil;

    @InjectMocks
    private FileServiceImpl fileService;

    private final String csvUrl = "http://example.com/yourfile.csv";
    private final String outputUrl = "http://example.com/upload";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileService = new FileServiceImpl(restTemplate, pdfService, fileUtil);
        ReflectionTestUtils.setField(fileService, "csvUrl", csvUrl);
        ReflectionTestUtils.setField(fileService, "outputUrl", outputUrl);

    }

    @Test
    void sendPdfToOutputServer_Success() throws IOException {
        byte[] pdfContent = "PDF content".getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(pdfContent);

        when(restTemplate.postForEntity(eq(outputUrl), any(HttpEntity.class), eq(String.class)))
                .thenReturn(new ResponseEntity<>(HttpStatus.OK));

        fileService.sendPdfToOutputServer(byteArrayInputStream);

        verify(restTemplate, times(1)).postForEntity(eq(outputUrl), any(HttpEntity.class), eq(String.class));
    }
}