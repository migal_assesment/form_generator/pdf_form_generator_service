package za.co.momentum.pdf_form_generator_service.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;
import za.co.momentum.pdf_form_generator_service.mappers.UserMapper;
import za.co.momentum.pdf_form_generator_service.models.CredentialsDto;
import za.co.momentum.pdf_form_generator_service.models.SignUpDto;
import za.co.momentum.pdf_form_generator_service.models.UserDto;
import za.co.momentum.pdf_form_generator_service.repo.UserRepository;
import za.co.momentum.pdf_form_generator_service.repo.entities.User;

import java.nio.CharBuffer;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UserMapper userMapper;
    private UserService userService;
    private User user;
    private UserDto userDto;
    private CredentialsDto credentialsDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userService = new UserServiceImpl(userRepository, passwordEncoder, userMapper);
    }

    @Test
    void testLoginSuccess() {

        user = new User( "Bruce", "Wayne", "brucewayne", "encryptedPassword");
        userDto = new UserDto(1L, "Bruce", "Wayne", "brucewayne", "token");
        credentialsDto = new CredentialsDto("brucewayne", "password".toCharArray());

        when(userRepository.findByLogin(credentialsDto.login())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(CharBuffer.wrap(credentialsDto.password()), user.getPassword())).thenReturn(true);
        when(userMapper.toUserDto(user)).thenReturn(userDto);

        UserDto result = userService.login(credentialsDto);

        assertNotNull(result);
        assertEquals(userDto.getId(), result.getId());
        assertEquals(userDto.getLogin(), result.getLogin());
        verify(userRepository, times(1)).findByLogin(credentialsDto.login());
        verify(passwordEncoder, times(1)).matches(CharBuffer.wrap(credentialsDto.password()), user.getPassword());
    }

    @Test
    void testLoginUserNotFound() {

        user = new User("Bruce", "Wayne", "brucewayne", "encryptedPassword");
        userDto = new UserDto(1L, "Bruce", "Wayne", "brucewayne", "token");
        credentialsDto = new CredentialsDto("brucewayne", "password".toCharArray());

        when(userRepository.findByLogin(credentialsDto.login())).thenReturn(Optional.empty());

        Exception exception = assertThrows(PdfFormException.class, () -> {
            userService.login(credentialsDto);
        });

        assertEquals("Unknown user", exception.getMessage());
        assertEquals(HttpStatus.NOT_FOUND, ((PdfFormException)exception).getHttpStatus());
    }

    @Test
    void testLoginInvalidPassword() {

        user = new User( "Bruce", "Wayne", "brucewayne", "encryptedPassword");
        userDto = new UserDto(1L, "Bruce", "Wayne", "brucewayne", "token");
        credentialsDto = new CredentialsDto("brucewayne", "password".toCharArray());

        when(userRepository.findByLogin(credentialsDto.login())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(CharBuffer.wrap(credentialsDto.password()), user.getPassword())).thenReturn(false);

        Exception exception = assertThrows(PdfFormException.class, () -> {
            userService.login(credentialsDto);
        });

        assertEquals("Invalid Password", exception.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, ((PdfFormException)exception).getHttpStatus());
    }

    @Test
    void testRegisterSuccess() {
        char[] password = "password".toCharArray();
        SignUpDto signUpDto = new SignUpDto("Bruce", "Wayne",  "johndoe", password);
        User user = new User("Bruce", "Wayne", "brucewayne", "encryptedPassword");
        UserDto userDto = new UserDto(1L, "Bruce", "Wayne", "brucewayne", "token");

        when(userRepository.findByLogin(signUpDto.login())).thenReturn(Optional.empty());
        when(userMapper.signUpToUser(signUpDto)).thenReturn(user);
        when(passwordEncoder.encode(CharBuffer.wrap(signUpDto.password()))).thenReturn("encryptedPassword");
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userMapper.toUserDto(user)).thenReturn(userDto);

        UserDto result = userService.register(signUpDto);

        assertNotNull(result);
        assertEquals(userDto.getLogin(), result.getLogin());
        verify(userRepository, times(1)).findByLogin(signUpDto.login());
        verify(userRepository, times(1)).save(any(User.class));
        verify(passwordEncoder, times(1)).encode(CharBuffer.wrap(signUpDto.password()));
    }

    @Test
    void testRegisterUserAlreadyExists() {
        char[] password = "password".toCharArray();
        SignUpDto signUpDto = new SignUpDto("Bruce", "Wayne",  "johndoe", password);
        User user = new User("Bruce", "Wayne", "brucewayne", "encryptedPassword");
        when(userRepository.findByLogin(signUpDto.login())).thenReturn(Optional.of(user));

        Exception exception = assertThrows(PdfFormException.class, () -> {
            userService.register(signUpDto);
        });

        assertEquals("User already exists", exception.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, ((PdfFormException)exception).getHttpStatus());
    }

}