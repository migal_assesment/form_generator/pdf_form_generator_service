package za.co.momentum.pdf_form_generator_service.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import za.co.momentum.pdf_form_generator_service.exceptions.PdfFormException;
import za.co.momentum.pdf_form_generator_service.util.FileUtil;

import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PdfService pdfService;

    @Mock
    private FileUtil fileUtil;

    private FileService fileService;

    @Captor
    private ArgumentCaptor<List<List<String>>> listArgumentCaptor;

    private final String csvUrl = "http://example.com/yourfile.csv";
    private String outputUrl = "http://example.com/upload";

    private  String getFileUrl = "http://localhost:3000/files";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileService = new FileServiceImpl(restTemplate, pdfService, fileUtil);
        ReflectionTestUtils.setField(fileService, "csvUrl", csvUrl);
        ReflectionTestUtils.setField(fileService, "outputUrl", outputUrl);
        ReflectionTestUtils.setField(fileService, "getFilenames", getFileUrl);

    }

    @Test
    void fetchCsvCreatePdfAndSend_Success() throws IOException {
        String csvContent = "Name, Age\nBruce Wayne, 30";
        when(restTemplate.getForObject(csvUrl, String.class)).thenReturn(csvContent);
        when(pdfService.createPdfForm(any())).thenReturn(new ByteArrayInputStream(new byte[0]));

        fileService.fetchCsvCreatePdfAndSend();

        verify(restTemplate, times(1)).getForObject(csvUrl, String.class);
        verify(pdfService, times(1)).createPdfForm(listArgumentCaptor.capture());
        List<List<String>> capturedArgument = listArgumentCaptor.getValue();
        assertEquals("Name", capturedArgument.get(0).get(0)); // More assertions can be added as needed
    }

    @Test
    void fetchCsvCreatePdfAndSend_EmptyContent() throws IOException {
        String csvContent = "";
        when(restTemplate.getForObject(csvUrl, String.class)).thenReturn(csvContent);
        doThrow(IOException.class).when(pdfService).createPdfForm(any());

        assertThrows(PdfFormException.class, () -> fileService.fetchCsvCreatePdfAndSend());
    }

    @Test
    void fetchCsvCreatePdfAndSend_IOException() throws IOException {
        String csvContent = null;
        when(restTemplate.getForObject(csvUrl, String.class)).thenReturn(csvContent);
        doThrow(IOException.class).when(pdfService).createPdfForm(any());

        assertThrows(RuntimeException.class, () -> fileService.fetchCsvCreatePdfAndSend());
    }

    @Test
    void getDataFromCsvFile_SimpleCsv() {

        String csvContent = "Name,Age\nBruce Wayne,30\nSelena Kyle,25";

        List<List<String>> result = fileService.getDataFromCsvFile(csvContent);

        assertEquals(3, result.size());

        assertEquals(List.of("Name", "Age"), result.get(0));

        assertEquals(List.of("Bruce Wayne", "30"), result.get(1));
        assertEquals(List.of("Selena Kyle", "25"), result.get(2));
    }

    @Test
    void getDataFromCsvFile_WithEmptyLines() {

        String csvContent = "\nName,Age\n\nBruce Wayne,30\n\nSelena Kyle,25\n";

        List<List<String>> result = fileService.getDataFromCsvFile(csvContent);

        assertEquals(3, result.size());

        assertEquals(List.of("Name", "Age"), result.get(0));

        assertEquals(List.of("Bruce Wayne", "30"), result.get(1));
        assertEquals(List.of("Selena Kyle", "25"), result.get(2));
    }


    @Test
    public void testGetFiles() {

        String filename = "test.pdf";

        String[] mockResponse = new String[1];
        mockResponse[0]= "test.pdf";
        when(restTemplate.getForObject(getFileUrl, String[].class)).thenReturn(mockResponse);


        List<String> fileContent = fileService.getFilenames();

        assertArrayEquals(Arrays.stream(mockResponse).toArray(), fileContent.toArray());
    }
}