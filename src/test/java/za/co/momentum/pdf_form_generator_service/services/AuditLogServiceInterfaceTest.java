package za.co.momentum.pdf_form_generator_service.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import za.co.momentum.pdf_form_generator_service.repo.AuditLogRepository;
import za.co.momentum.pdf_form_generator_service.repo.entities.AuditLog;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class AuditLogServiceInterfaceTest {

    @Mock
    private AuditLogRepository auditLogRepository;

    private AuditLogServiceInterface auditLogService;

    @Captor
    private ArgumentCaptor<AuditLog> auditLogCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        auditLogService = new AuditLogServiceInterface(auditLogRepository);
    }

    @Test
    void logChange_createsAndSavesAuditLogCorrectly() {
        String tableName = "User";
        Long entityId = 1L;
        String operationType = "CREATE";
        String details = "User created";
        String changedBy = "admin";

        auditLogService.logChange(tableName, entityId, operationType, details, changedBy);

        verify(auditLogRepository).save(auditLogCaptor.capture());
        AuditLog capturedAuditLog = auditLogCaptor.getValue();

        assertEquals(capturedAuditLog.getTableName(), tableName);
        assertEquals(capturedAuditLog.getEntityId(), entityId);
        assertEquals(capturedAuditLog.getOperationType(), operationType);
        assertEquals(capturedAuditLog.getDetails(),details);
        assertEquals(capturedAuditLog.getChangedBy(),changedBy);
        assertNotNull(capturedAuditLog.getChangeTime());
    }
}