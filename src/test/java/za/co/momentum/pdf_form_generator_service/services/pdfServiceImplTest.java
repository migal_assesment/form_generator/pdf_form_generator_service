package za.co.momentum.pdf_form_generator_service.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class pdfServiceImplTest {

    PdfServiceImpl pdfServiceImpl;

    @BeforeEach
    void setUp() {
        pdfServiceImpl = new PdfServiceImpl();
    }

    @Test
    void createPdfForm() throws IOException {
        List<List<String>> rows = new ArrayList<>();
        rows.add(Arrays.asList("First Name", "Last Name", "Age", "Gender"));
        this.pdfServiceImpl.createPdfForm(rows);
    }

    @Test
    void createPdfPage() {
    }
}